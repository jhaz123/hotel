<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterRoomRoomTypeId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::table('rooms', function (Blueprint $table) {

		    $table->dropColumn('type');
		    $table->integer('room_type_id')->unsigned();
		    $table->foreign('room_type_id')
			    ->references('id')
			    ->on('room_types')
			    ->onDelete('cascade');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::table('rooms', function (Blueprint $table) {

		    $table->string('type');
		    $table->dropForeign('room_types_room_type_id_foreign');
		    $table->dropColumn('room_type_id');
	    });
    }
}
