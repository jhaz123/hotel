<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomsTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('rooms', function (Blueprint $table) {
		    $table->increments('id');
		    $table->integer('floor_id')->unsigned();
		    $table->foreign('floor_id')
			    ->references('id')
			    ->on('floors')
			    ->onDelete('cascade');
		    $table->integer('room_number');
		    $table->dateTime('maintenance_start_date')->nullable();
		    $table->dateTime('maintenance_end_date')->nullable();
		    $table->tinyInteger('hasGolfView');
		    $table->string('type');
		    $table->timestamps();
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::dropIfExists('rooms');
    }
}
