<?php

use App\Floor;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FloorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::statement('SET FOREIGN_KEY_CHECKS=0');
    	Floor::truncate();
    	for($ctr = 1; $ctr <= 5; $ctr++){
    		$d = [
    		    'floor_number' => $ctr
		    ];
		    Floor::insert($d);
	    }

	    DB::statement('SET FOREIGN_KEY_CHECKS=1');

    }
}
