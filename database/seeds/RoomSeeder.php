<?php

use App\Floor;
use App\Room;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoomSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    DB::statement('SET FOREIGN_KEY_CHECKS=0');
	    Room::truncate();
	    for($ctr = 1; $ctr <= 3; $ctr++){

		    $d = [
			    'floor_id' => 1,
			    'room_number' => $ctr,
			    'hasGolfView' => 1,
			    'room_type_id' => 1
		    ];
		    Room::insert($d);
	    }

	    for($ctr = 4; $ctr <= 6; $ctr++){

		    $d = [
			    'floor_id' => 2,
			    'room_number' => $ctr,
			    'hasGolfView' => 0,
			    'room_type_id' => 2
		    ];
		    Room::insert($d);
	    }

	    DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
