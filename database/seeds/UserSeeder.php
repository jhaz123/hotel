<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		DB::statement('SET FOREIGN_KEY_CHECKS=0');
//		User::truncate();

		$user = [
			'name'         => 'Receptionist ABC',
			'email'        => 'receptionist@gmail.com',
			'password'     => bcrypt('123123'),
			'user_role_id' => getenv('USER_ROLE_ID_RECEPTIONIST'),
		];
		User::insert($user);
		DB::statement('SET FOREIGN_KEY_CHECKS=1');
	}
}
