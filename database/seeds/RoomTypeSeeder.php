<?php

use App\RoomType;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoomTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    DB::statement('SET FOREIGN_KEY_CHECKS=0');
	    RoomType::truncate();

	    $rt = ['type' => 'single'];
	    RoomType::insert($rt);
	    $rt = ['type' => 'family'];
	    RoomType::insert($rt);
	    $rt = ['type' => 'suit'];
	    RoomType::insert($rt);
	    DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
