<?php

use App\UserRole;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserRoleSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
//        UserRole::truncate();
		DB::statement('SET FOREIGN_KEY_CHECKS=0');
		UserRole::truncate();

		$userRoles = ['type' => 'customer'];
		UserRole::insert($userRoles);
		$userRoles = ['type' => 'receptionist'];
		UserRole::insert($userRoles);
		$userRoles = ['type' => 'admin'];
		UserRole::insert($userRoles);
		DB::statement('SET FOREIGN_KEY_CHECKS=1');

	}
}
