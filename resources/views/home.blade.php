@extends('layouts.app')

@section('content')
<div id="booking" class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif


                </div>


                @if(Auth::user()->user_role_id == getenv('USER_ROLE_ID_CUSTOMER'))
                     <form-booking>   </form-booking>
                @elseif (Auth::user()->user_role_id == getenv('USER_ROLE_ID_RECEPTIONIST'))

                    <form-booking-receptionist> </form-booking-receptionist>

                @endif

            </div>
        </div>
    </div>
</div>
@endsection
