import axios from 'axios'


// export const getAvailableRooms = (d) => {
//
//
//     const postData = JSON.parse(JSON.stringify(d, (k, v) => v === undefined ? null : v));
//
//     console.info(postData);
//     return axios.post(`get-available-rooms`, postData)
//         .then(res => res.data)
//         .catch((err) => {
//             console.error('Unable to validate access code');
//             return err;
//         })
// }

export const getRoomTypes = () =>
    axios.get(`get-room-types`).then(res => res.data)


export const getAvailableRooms = (roomTypeId) =>
    axios.get(`get-available-rooms/` + roomTypeId).then(res => res.data)

export const bookReservation = (d) => {

    const postData = JSON.parse(JSON.stringify(d, (k, v) => v === undefined ? null : v));

    return axios.post(`book`, postData)
        .then(res => res.data)
        .catch((err) => {
            console.error('Unable to book reservation');
            return err;
        })
}

export const getBookings = () =>
    axios.get(`get-bookings`).then(res => res.data)

export const toggleIsBookedCustomer = (d) => {

    const postData = JSON.parse(JSON.stringify(d, (k, v) => v === undefined ? null : v));

    return axios.post(`toggle-is-booked-customer`, postData)
        .then(res => res.data)
        .catch((err) => {
            console.error('Unable to change booking status');
            return err;
        })
}