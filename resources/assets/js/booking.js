import common from './common';
// import eventBus from './eventBus';

import FormBooking from './components/customer/FormBooking.vue'
import FormBookingReceptionist from './components/receptionist/FormBookingReceptionist.vue'


const booking = new common.Vue({
    el: '#booking',

    components: {
        'form-booking': FormBooking,
        'form-booking-receptionist': FormBookingReceptionist
    }
});