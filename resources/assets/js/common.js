/*
* Common initializations thorough out the app
* */

import Vue from 'vue'
import axios from 'axios'
import * as apis from './services/api'


window.Vue = Vue;

axios.defaults.baseURL = document.location.origin;

Vue.prototype.$api = apis

// Add a response interceptor
axios.interceptors.response.use(function (response) {
    // Do something with response data
    return response;
}, function (error) {

    //If user encounteres an unauthorized error (session timeout)
    if (error.response.status == 401){
        window.location.reload();
    }

    return Promise.reject(error);
});

export default {Vue}