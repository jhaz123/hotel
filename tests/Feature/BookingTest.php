<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BookingTest extends TestCase
{

	/**
	 * Check returned data of room types
	 */
	public function testRoomTypesReturnData()
    {

	    $roomTypes = $this->call('GET', 'get-room-types')->decodeResponseJson();

	    //Check if returning status is success
	    $this->assertTrue($roomTypes['success']);

	    //Check if return data key exists since this will be used to display json data
	    $this->assertArrayHasKey( 'returnData', $roomTypes);

	    //Should return more than 0 data
	    $this->assertGreaterThan(0, count($roomTypes['returnData']));

    }


	/**
	 * Check returned data of rooms
	 */
//	public function testRoomsReturnData()
//	{
//
//		$roomTypes = $this->call('GET', 'get-rooms')->decodeResponseJson();
//
//		//Check if returning status is success
//		$this->assertTrue($roomTypes['success']);
//
//		//Check if return data key exists since this will be used to display json data
//		$this->assertArrayHasKey( 'returnData', $roomTypes);
//
//		//Should return more than 0 data
//		$this->assertGreaterThan(0, count($roomTypes['returnData']));
//
//	}
}
