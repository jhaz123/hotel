<?php

namespace App\Http\Controllers;

use App\Booking;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class BookingController extends Controller {
	/**
	 * @var Booking
	 */
	private $booking;

	/**
	 * BookingController constructor.
	 * @param Booking $booking
	 */
	public function __construct(Booking $booking) {

		$this->booking = $booking;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @param Request $request
	 * @return void
	 */
	public function create() {
		echo 'here';
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$data = $request->all();

		$isValid = $this->validateBeforeStore($data);

		$code = 200;
		$returnData = [];

		//If not valid, return error message and status
		if (!$isValid['success']) {

			return response()->json([
				'success'    => false,
				'message'    => $isValid['message'],
				'returnData' => $returnData,
			], $code);

			return;
		}

		try {

			$userId = Auth::user()->id;


			$createData = [
				'user_id' => $userId,
				'room_id' => $data['room_id'],
				'is_booked' => 0,
				'created_at' => Carbon::now(),
				'updated_at' => Carbon::now(),
			];

			if ($data['has_dinner_booked']) {

				$createData['dinner_time'] = Carbon::parse(date($data['dinner_time']));
				$createData['has_dinner_booked'] = $data['has_dinner_booked'];
			}


			$this->booking->create($createData);

			$code = 200;
			$message = 'success';
			$success = true;

		} catch (Exception $e) {
			$message = $e->getMessage();
			$code = 500;
			$success = false;

			$msg = $this->getErrorMessage($e);

			Log::error($msg);
		}

		return response()->json([
			'success' => $success,
			'message' => $message,
		], $code);

	}


	/**
	 * Display the specified resource.
	 *
	 * @param int $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param int $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param int $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param int $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		//
	}

	/**
	 * Validate data before storing in database
	 * @param $data
	 * @return array
	 */
	public function validateBeforeStore($data) {

		$response = [
			'success' => true,
			'message' => '',
		];

		$messages = [
			'room_id.required' => 'Please select a valid room',
		];

		$validator = Validator::make($data, [

			'room_id' => 'required',

		], $messages);

		$errors = $validator->errors()->all();

		if (count($errors) > 0) {
			$response = [
				'success' => false,
				'message' => $errors[0],
			];
		}

		return $response;

	}

	/**
	 * Get reservations for a date given
	 */
	public function getBookings() {


		try {

			$data = $this->booking
				->with('user')
				->get();

			$code = 200;
			$message = 'success';
			$success = true;
			$returnData = $data;

		} catch (Exception $e) {
			$message = $e->getMessage();
			$code = 500;
			$success = false;
			$returnData = [];
		}

		return response()->json([
			'success'    => $success,
			'message'    => $message,
			'returnData' => $returnData,
		], $code);


	}

	/**
	 * @param Request $request
	 */
	public function toggleIsBookedCustomer(Request $request) {
		$data = $request->all();

		try {

			$updateData=[
				'is_booked' => ($data['is_booked'] == '1' ? 0 : 1)
			];

			$data = $this->booking
				->where('id', $data['id'])
				->update($updateData);

			$code = 200;
			$message = 'success';
			$success = true;

		} catch (Exception $e) {
			$message = $e->getMessage();
			$code = 500;
			$success = false;
		}

		return response()->json([
			'success'    => $success,
			'message'    => $message,
		], $code);
	}
}
