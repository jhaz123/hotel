<?php

namespace App\Http\Controllers;

use App\Booking;
use App\Room;
use App\RoomType;
use Illuminate\Http\Request;

class RoomsController extends Controller
{
	/**
	 * @var Room
	 */
	private $room;
	/**
	 * @var RoomType
	 */
	private $roomType;
	/**
	 * @var Booking
	 */
	private $booking;

	/**
	 * RoomsController constructor.
	 * @param Room $room
	 * @param RoomType $roomType
	 * @param Booking $booking
	 */
	public function __construct(Room $room, RoomType $roomType, Booking $booking){

		$this->room = $room;
		$this->roomType = $roomType;
		$this->booking = $booking;
	}

	public function getRoomTypes() {

		$returnData = [];

		try {

			$returnData = $this->roomType->all();

			$code = 200;
			$message = 'success';
			$success = true;

		} catch (\Exception $e) {
			$message = $e->getMessage();
			$code = 500;
			$success = false;

		}

		return response()->json([
			'success' => $success,
			'message' => $message,
			'returnData' => $returnData
		], $code);

	}


	/**
	 * Get rooms in database that are not yet booked
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function getAvailableRooms($roomTypeId) {

		$returnData = [];


		try {

			$bookedRooms = $this->booking->pluck('room_id')->toArray();

			$availableRooms = $this->room
				->whereNotIn('id', $bookedRooms)
				->where('maintenance_start_date', null)
				->where('maintenance_end_date', null)
				->where('room_type_id', $roomTypeId)
				->get();


			$returnData = $availableRooms;

			$code = 200;
			$message = 'success';
			$success = true;

		} catch (\Exception $e) {
			$message = $e->getMessage();
			$code = 500;
			$success = false;

		}

		return response()->json([
			'success' => $success,
			'message' => $message,
			'returnData' => $returnData
		], $code);

	}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        return $this->room->
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
