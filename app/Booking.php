<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
	protected $fillable = [
		'user_id', 'room_id', 'has_dinner', 'dinner_time', 'has_dinner_booked'
	];

	public function user() {
		return $this->belongsTo('App\User');
	}
}
