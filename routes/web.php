<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
$router->get('logout', 'Auth\LoginController@logout');

Route::get('/home', 'HomeController@index')->name('home');



//Customer flow..
$router->get('get-room-types', 'RoomsController@getRoomTypes');
$router->get('get-available-rooms/{roomTypeId}', 'RoomsController@getAvailableRooms');
$router->resource('book', 'BookingController');
$router->get('book-customer/{userId}', 'BookingController@show');

//Receptionist flow...
$router->get('get-bookings', 'BookingController@getBookings');
$router->post('toggle-is-booked-customer', 'BookingController@toggleIsBookedCustomer');


//Customer survey flow...
$router->resource('customer-survey', 'SurveyController');